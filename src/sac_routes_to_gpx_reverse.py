"""
This script converts coordinate data from a JSON file into a GPX file format.

Follow up script to reverse the coordinates in the gpx file
in case the descent is described as an alternative ascent.

The output GPX file will contain the coordinates in WGS84 format, suitable for use with GPS software.
"""

import os
import json
import chardet
from pyproj import Transformer
from xml.etree.ElementTree import Element, SubElement, ElementTree

# Set up directories relative to the script location
script_dir = os.path.dirname(os.path.abspath(__file__))
parent_dir = os.path.dirname(script_dir)
data_dir = os.path.join(parent_dir, 'data')
output_dir = os.path.join(parent_dir, 'output')

# Ensure the output directory exists
os.makedirs(output_dir, exist_ok=True)

# List .txt files in the data directory
txt_files = [f for f in os.listdir(data_dir) if f.endswith('.txt')]

if not txt_files:
    print("No .txt files found in the data directory.")
    exit()

# Ask the user to select a file
print("Select a .txt file to process:")
for idx, file_name in enumerate(txt_files):
    print(f"{idx + 1}. {file_name}")

selected_idx = int(input("Enter the number of the file: ")) - 1
selected_file = txt_files[selected_idx]
input_file_path = os.path.join(data_dir, selected_file)

# Determine output file names
base_name = os.path.splitext(selected_file)[0]
gpx_output_path = os.path.join(output_dir, f"{base_name}.gpx")
route_info_output_path = os.path.join(output_dir, f"{base_name}_route_info.txt")

# Initialize the transformer from LV95 to WGS84
transformer = Transformer.from_crs("EPSG:2056", "EPSG:4326", always_xy=True)

# Function to detect file encoding
def detect_file_encoding(file_path):
    with open(file_path, 'rb') as file:
        raw_data = file.read()
        result = chardet.detect(raw_data)
        encoding = result['encoding']
        confidence = result['confidence']
        print(f"Detected encoding: {encoding} (confidence: {confidence})")
        return encoding

# Step 1: Detect encoding of the JSON file
encoding = detect_file_encoding(input_file_path)

# Step 2: Load JSON data from file using detected encoding
with open(input_file_path, 'r', encoding=encoding) as file:
    data = json.load(file)

# Step 3: Extract and concatenate coordinates from all segments
all_coordinates = []
for segment in data.get("segments", []):
    if segment.get("geom") is not None:
        coordinates = segment["geom"].get("coordinates", [])
        all_coordinates.extend(coordinates)

# Step 4: Reverse the order of coordinates
reversed_coordinates = all_coordinates[::-1]

# Step 5: Convert LV95 coordinates to WGS84
converted_coordinates = [transformer.transform(x, y) for x, y in reversed_coordinates]

# Step 6: Create a GPX file with the coordinates
gpx = Element('gpx', version="1.1", creator="GPX Creator", xmlns="http://www.topografix.com/GPX/1/1")

trk = SubElement(gpx, 'trk')
trkseg = SubElement(trk, 'trkseg')

for lon, lat in converted_coordinates:
    trkpt = SubElement(trkseg, 'trkpt', lat=str(lat), lon=str(lon))

# Write the GPX file
tree = ElementTree(gpx)
tree.write(gpx_output_path, xml_declaration=True, encoding='utf-8')
print(f"GPX file has been saved to {gpx_output_path}")

# Step 7: Extract and save route description
route_info = data.get("route_info", {}).get("list", [])
with open(route_info_output_path, 'w', encoding='utf-8') as route_file:
    route_file.write("# Route description\n")
    for item in route_info:
        label = item.get("label", "")
        value = item.get("value", "")
        discipline = item.get("discipline", "")
        route_file.write(f"{label}: {value}\n")
        if discipline:
            route_file.write(f"Discipline: {discipline}\n")
print(f"Route description has been saved to {route_info_output_path}")
