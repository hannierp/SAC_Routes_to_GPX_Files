# SAC and schweizmobil.ch Routes to GPX Files

This project converts routes from the SAC Tourenportal <https://www.sac-cas.ch/de/huetten-und-touren/sac-tourenportal/> and schweizmobil.ch into GPX tracks, suitable for use with GPS software.

## Directory Structure

Ensure the following directory structure is present in the working directory, as the scripts depend on it:
- ./data
- ./output
- ./src

- Place the following scripts in the `./src` folder.
	- *./src/gpx_merger.py*
	- *./src/sac_routes_to_gpx.py*
	- *./src/sac_routes_to_gpx_reverse.py*
	- *./src/schweizmobil_scrape_and_convert_to_gpx.py*

- Output files (GPX and route descriptions) will be saved in the `./output` folder.

## Fetching Route Data

To fetch route data from the SAC Tourenportal, follow these steps:

1. **Login** to the SAC Tourenportal.
2. Navigate to a tour page, such as:  
   <https://www.sac-cas.ch/de/huetten-und-touren/sac-tourenportal/weissmies-2192/hochtouren/von-der-almagellerhuette-sac-ueber-den-sse-grat-2412/>
3. Open the map and click on the route in the list to view details.
4. Open the **Developer Tools** in your browser (press `F12`).
5. Go to the **Network** tab and select **Fetch/XHR**.
6. On the map, click on any section of the route.
7. In the list of network requests, search for a request with the format `?type=`.
   - This request will contain the route description and coordinates.
8. Right-click the request and choose **Copy response**.
9. Save as txt file with the name of the tour in the `./data` folder
   - You should end up with a URL similar to this:  
     <https://www.sac-cas.ch/de/?type=1567765346410&tx_usersaccas2020_sac2020[routeId]=2412>

## Running the Main Script

To convert the fetched route data into GPX format:

1. Open a command prompt (Windows) or terminal.
2. Navigate to the `./src` directory.
3. Run the script using the command:
	```bash
		python sac_routes_to_gpx.py
	```
4. The script will prompt you for:
	- The txt file that you saved in the `./data` folder.

### Output

The script generates two output files:

- A **GPX file** containing route coordinates in WGS84 format.
- A **text file** containing the route description.

The GPX file can be uploaded to GPS software or devices (e.g., outdooractive.com, Suunto, or Garmin watches).

## Handling Descent Routes

Sometimes, descent routes are described as ascents from a different starting point. To reverse the order of the coordinates (useful for descent routes), use the following script:

1. Run the reverse route script using the command in command prompt (Windows):
	```bash
		python sac_routes_to_gpx_reverse.py
	```
This will reverse the order of the coordinates and generate a new GPX file.

## Merging Ascent and Descent Routes

To merge two GPX files (ascent and descent):

1. Run the GPX merger script:
	```
		python gpx_merger.py
	```
2. The script will prompt you to select the GPX files to combine.
3. The merged GPX file will be saved in the `./output` folder with a name based on the selected files.


# schweizmobil.ch Routes to GPX Files

The schweizmobil.ch route data structure differs from that of the SAC Tourenportal. Therefore, use the script *schweizmobil_scrape_and_convert_to_gpx.py* to process schweizmobil routes.

- Output files (GPX and route descriptions) will be saved in the `./output` folder.

## Fetching Route Data

To fetch route data from the schweizmobil.ch portal, follow these steps:

1. Navigate to a tour page, such as:  
   <https//schweizmobil.ch/en/mountainbiking-in-switzerland/route-41/stage-1>
2. Open the **Developer Tools** in your browser (press `F12`).
3. Open the map and click on the route you are interested in.
   - As soon as you click on the route, a request will appear in the **Network** tab (under **Fetch/XHR**) that starts with *query?restriction*.  
   3. Open the map and click on the route you are interested in.
   - As soon as you click on the route, a request will appear in the **Network** tab (under **Fetch/XHR**) that starts with *query?restriction*.  
   Example URL:  
   <https://schweizmobil.ch/api/4/feature/query?restriction=15&attributes=yes&type=etappen&land=all&category=all&simplify=5&translated=true&language=en&layers=MtblandEtappenNational%2CMtblandEtappenRegional%2CMtblandEtappenLokal%2CDetour_mtbland%2CLogo_mtbland%2CHighlight&bbox=608455%2C132253%2C608783%2C132581>
   - This request contains a *FeatureCollection* with multiple routes, not just the one you're interested in. The script will prompt you to choose the specific route.
4. Right-click on the *query?restriction* request and choose **Copy as URL**.
   - You will paste this URL into the script.

## Running the *schweizmobil_scrape_and_convert_to_gpx.py* Script

To convert the fetched schweizmobil route data into GPX format:

1. Open a command prompt (Windows) or terminal.
2. Navigate to the `./src` directory.
3. Run the script using the command:
	```bash
	python schweizmobil_scrape_and_convert_to_gpx.py
	```
4. The script will prompt you for:
	- The URL you copied earlier.
	- To select the specific route you want to convert from the list of available routes.
	- A name for the output file (without extension).

### Output

The script generates two output files:

- A **GPX file** containing the route coordinates in WGS84 format.
- A **text file** containing the route description.

The GPX file can be uploaded to GPS software or devices (e.g., outdooractive.com, Suunto, or Garmin watches).

## Additional Notes - Requirements

- Ensure you have Python 3.x and the necessary libraries installed (`requests`, `chardet`, `pyproj`, etc.).
- The `.gpx` files generated by these scripts are compatible with most GPS platforms and devices.
- The scripts were tested on Windows 10.
- Python version 3.11.5 was used for testing.