"""
### schweizmobil_scrape_and_convert_to_gpx.py

**Description:**

This script performs two main functions:

1. **Data Fetching from URL**:
   - It downloads JSON route data from a user-provided URL from schweizmobil.ch.
   - The user is prompted to enter the full fetch URL, to select the route and a desired output filename.
   - The script sends a GET request with specific headers to retrieve the data from the provided URL.
   - The fetched data is processed in-memory without saving it to an intermediate text file.

2. **Route Data Processing and GPX Generation**:
   - Extracts route coordinates and information from the fetched JSON data.
   - Converts the route coordinates from LV95 (EPSG:2056) to WGS84 (EPSG:4326) format, making them suitable for GPS software.
   - Prompts the user to select a route from multiple available options if more than one is present in the data.
   - Generates two output files:
     - A **GPX file** containing the converted route coordinates, including elevation data if available.
     - A **text file** containing the route description, including details such as the start and end points and an abstract or summary of the route.

**Functionality:**
- Prompts the user for the URL of the JSON route data and a filename for the output files.
- Fetches and processes the JSON data in real-time.
- Extracts route coordinates and relevant information from the JSON data.
- Converts the coordinates from the LV95 Swiss coordinate system to the WGS84 global coordinate system.
- Generates both a GPX file and a route description text file.

**Usage:**
- Ensure the required Python libraries (`requests`, `pyproj`) are installed
  
"""


import os
import json
import requests
from pyproj import Transformer
from xml.etree.ElementTree import Element, SubElement, ElementTree

# Set up directories relative to the script location
script_dir = os.path.dirname(os.path.abspath(__file__))
parent_dir = os.path.dirname(script_dir)
output_dir = os.path.join(parent_dir, 'output')

# Ensure the output directory exists
os.makedirs(output_dir, exist_ok=True)

# Ask the user to input the URL and filename for the output files
fetch_url = input("Enter the full URL to fetch route data: ")
desired_filename = input("Enter the desired filename (without extension): ")

gpx_output_path = os.path.join(output_dir, f"{desired_filename}.gpx")
route_info_output_path = os.path.join(output_dir, f"{desired_filename}_route_info.txt")

# Step 1: Fetch data from the URL
response = requests.get(fetch_url, headers={"User-Agent": "Mozilla/5.0"})
if response.status_code != 200:
    print("Failed to fetch data from the URL.")
    exit()

# Load JSON data directly from the response
data = response.json()

# Initialize the transformer from LV95 to WGS84
transformer = Transformer.from_crs("EPSG:2056", "EPSG:4326", always_xy=True)

# Function to display route choices
def display_route_choices(features):
    route_options = []
    for idx, feature in enumerate(features):
        properties = feature.get("properties", {})
        start = properties.get("start", "Unknown Start")
        end = properties.get("end", "Unknown End")
        route_options.append({
            "index": idx,
            "start": start,
            "end": end,
            "abstract": properties.get("abstract", "No description available.")
        })
    return route_options

# Step 2: Extract route options and ask user to select
routes = display_route_choices(data.get("features", []))

if not routes:
    print("No routes found in the data.")
    exit()

print("Available routes:")
for i, route in enumerate(routes):
    print(f"{i + 1}. {route['start']} -> {route['end']}")

selected_route_idx = int(input("Select a route to convert (enter the number): ")) - 1
selected_route = routes[selected_route_idx]
selected_features = data["features"][selected_route["index"]]

# Initialize a list for collecting coordinates
all_coordinates = []

# Step 3: Extract coordinates from the selected route
def extract_coordinates_from_feature(feature):
    coordinates = []
    geometry_type = feature.get("geometry", {}).get("type")
    coords = feature.get("geometry", {}).get("coordinates", [])
    
    if geometry_type == "Point":
        coordinates.append(coords)
    
    elif geometry_type == "LineString":
        coordinates.extend(coords)
    
    elif geometry_type == "MultiLineString":
        for line in coords:
            coordinates.extend(line)
    
    return coordinates

# Extract coordinates from the selected feature
all_coordinates = extract_coordinates_from_feature(selected_features)

# Step 4: Adjust coordinates by adding the offset if they are small (probably LV03)
def adjust_coordinates(coords):
    adjusted = []
    for coord in coords:
        x, y, *rest = coord  # Extract x, y, and optional z (elevation)
        if x < 2_000_000 and y < 1_000_000:
            # Add the offsets to convert to LV95
            x += 2_000_000
            y += 1_000_000
        adjusted.append((x, y, *rest))
    return adjusted

# Adjust the coordinates if needed
adjusted_coordinates = adjust_coordinates(all_coordinates)

# Step 5: Convert LV95 coordinates to WGS84
converted_coordinates = [(transformer.transform(x, y), rest[0] if rest else None) for (x, y, *rest) in adjusted_coordinates]

# Step 6: Create a GPX file with the coordinates, including elevation if available
gpx = Element('gpx', version="1.1", creator="GPX Creator", xmlns="http://www.topografix.com/GPX/1/1")

trk = SubElement(gpx, 'trk')
trkseg = SubElement(trk, 'trkseg')

for (lon, lat), ele in converted_coordinates:
    trkpt = SubElement(trkseg, 'trkpt', lat=str(lat), lon=str(lon))
    if ele is not None:
        ele_tag = SubElement(trkpt, 'ele')
        ele_tag.text = str(ele)

# Write the GPX file
tree = ElementTree(gpx)
tree.write(gpx_output_path, xml_declaration=True, encoding='utf-8')
print(f"GPX file has been saved to {gpx_output_path}")

# Step 7: Extract and save route description (including abstract)
route_info = selected_features.get("properties", {})
with open(route_info_output_path, 'w', encoding='utf-8') as route_file:
    route_file.write("# Route description\n")
    route_file.write(f"Start: {route_info.get('start', 'Unknown')}\n")
    route_file.write(f"End: {route_info.get('end', 'Unknown')}\n")
    route_file.write(f"Abstract: {route_info.get('abstract', 'No description available.')}\n")
print(f"Route description has been saved to {route_info_output_path}")
