"""
This Python script combines two or more GPX files into a single GPX file.

**Functionality:**
- The script reads multiple GPX files from a specified directory.
- It extracts the track segments (`<trkseg>`) from each GPX file.
- All extracted track segments are merged into a single GPX file.
- The output is a combined GPX file that contains all the tracks from the input files.

**Usage:**
- Ensure the GPX files to be combined are located in the specified directory.
- The script prompts the user to select GPX files to combine.
- The script outputs the combined GPX file to the same directory with a name based on the selected files.

**Requirements:**
- Python 3.x
- The script uses the `xml.etree.ElementTree` module, which is included in the Python standard library.

**Example:**
- Input GPX files: `weissmiss.gpx`, `weissmiss_descent.gpx`
- Output GPX file: `weissmiss_weissmiss_descent_combined_tracks.gpx`

This script is useful for merging multiple GPX tracks, such as when combining ascent and descent tracks into a single file for better route analysis or visualization.
"""

import os
from xml.etree.ElementTree import ElementTree, parse, Element, SubElement

# Set up directories relative to the script location
script_dir = os.path.dirname(os.path.abspath(__file__))
parent_dir = os.path.dirname(script_dir)
output_dir = os.path.join(parent_dir, 'output')

# Ensure the output directory exists
os.makedirs(output_dir, exist_ok=True)

# List .gpx files in the output directory
gpx_files = [f for f in os.listdir(output_dir) if f.endswith('.gpx')]

if not gpx_files:
    print("No .gpx files found in the output directory.")
    exit()

# Ask the user to select GPX files to combine
print("Select GPX files to combine:")
for idx, file in enumerate(gpx_files):
    print(f"{idx + 1}. {file}")

selected_indices = input("Enter the numbers of the files to combine, separated by commas (e.g., 1,2): ").split(',')
selected_files = [gpx_files[int(i.strip()) - 1] for i in selected_indices]

if not selected_files:
    print("No files selected. Exiting.")
    exit()

# Generate the output filename
combined_filename = "_".join([os.path.splitext(f)[0] for f in selected_files]) + "_combined_tracks.gpx"
output_gpx_file = os.path.join(output_dir, combined_filename)

# Initialize the root for the new GPX file
combined_gpx = Element('gpx', version="1.1", creator="GPX Combiner", xmlns="http://www.topografix.com/GPX/1/1")
trk = SubElement(combined_gpx, 'trk')

# Function to merge GPX files
def merge_gpx_files(gpx_files):
    for gpx_file in gpx_files:
        gpx_path = os.path.join(output_dir, gpx_file)
        
        # Parse each GPX file
        tree = parse(gpx_path)
        root = tree.getroot()
        
        # Find all track segments in the GPX file
        for trkseg in root.findall('.//{http://www.topografix.com/GPX/1/1}trkseg'):
            # Append each track segment to the combined GPX file
            trk.append(trkseg)

# Step 1: Merge the GPX files
merge_gpx_files(selected_files)

# Step 2: Write the combined GPX content to the output file
tree = ElementTree(combined_gpx)
tree.write(output_gpx_file, xml_declaration=True, encoding='utf-8')

print(f"Combined GPX file has been saved to {output_gpx_file}")
